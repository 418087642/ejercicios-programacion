#include <stdio.h>
int main()
{
    //SE INGRESA UN NUMERO ENTRE 1 Y 50 Y SE SUMA HASTA LLEGAR A ESE NUMERO
    int numD = 0, i, suma = 0;
    printf("Digita un numero entre 1 y 50: ");
    do
    {
        scanf("%d", &numD);
        if(numD <= 1 || numD >= 50)
            printf("Digita nuevamente: ");
    }while(numD <= 1 || numD >= 50);
    //printf("sali");
    for(i=1;i <= numD; i++)
    {
        suma += i;
    }
    printf("%d", suma);
    return 0;
}