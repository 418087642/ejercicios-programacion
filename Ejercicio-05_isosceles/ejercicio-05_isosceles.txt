#include <stdio.h>
#define  LI 2
//PERIMETRO DE TRIANGULO ISOCELES
int main(){
    float ladoIgual = 0, ladoDif = 0, perimetro = 0;
    printf("Lado Igual(cm): ");
    scanf("%f", &ladoIgual);
    printf("Lado Diferente: ");
    scanf("%f", &ladoDif);
    perimetro = LI*ladoIgual + ladoDif;
    printf("Perimetro: %.2f", perimetro);
    return 0;
}